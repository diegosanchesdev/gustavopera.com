<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gustavopera_com');

/** MySQL database username */
//define('DB_USER', 'gustavop_wp208');
define('DB_USER', 'root');

/** MySQL database password */
//define('DB_PASSWORD', 'gp85SN-54!');
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dzr8r2ulqs6ssicf7adpagf1oqiz0cbf6lecdx0ycl1pbcck5zryr4tbgxgriebx');
define('SECURE_AUTH_KEY',  'c9tfjahrquo3ugx1l7d9nhh8iie1k1vqoahjtosqjmr6xxth3xf3beojwtugc4ls');
define('LOGGED_IN_KEY',    'f3n5gjfvix5pyvcgoe8kh7hutsdxluubrpw6l1yvsfuiwcknfx1detdhtkpj6bus');
define('NONCE_KEY',        'wiz6ffdsmrvem5k0abvzguhu80plozi8enxmugnuotpcgj0pu1raxoeroz6uygxp');
define('AUTH_SALT',        '8vnny7zhue76ncxlkx8pnvmzqerpz0goxadosqeri9v4mwecx4t1fg5qhpsayfs7');
define('SECURE_AUTH_SALT', 'camichgdppldu0rwqprra1py2mi8evkrxsthvpu8kv7obasizp1llujl0qvwf8l2');
define('LOGGED_IN_SALT',   'q1ikvyytahipz5bixoa0ksphme8npsq5woyx8i9qvmzvtjihlqh6vdday64qf1qp');
define('NONCE_SALT',       '07vvd8psdckqbckhe8mjxkvkkjmoo2alimbjmn7yti7ayxruodyviwoagmt8wajq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpvi_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
