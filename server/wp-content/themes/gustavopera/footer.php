<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Morphology Lite
 */

?>

	



</div><!-- .site -->

<?php wp_footer(); ?>


<script type="text/javascript">
	jQuery(document).on('ready', function() {
		jQuery('#thumbHomeReel').on('click', function() {

			jQuery('#videoHomeReel iframe').attr('src', jQuery(this).attr('href'));
			jQuery('#videoHomeReel').fadeIn(300);
			return false;
		});

		jQuery('#closeHomeReel').on('click', function() {

			jQuery('#videoHomeReel iframe').attr('src', '');
			jQuery('#videoHomeReel').fadeOut(300);
			return false;
		});
	});
</script>
</body>
</html>