<?php
/**
 * Template Name: Home 2018
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Morphology Lite
 */

get_header(); ?>

<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<h1 id="titleShowReel" class="entry-title"><span>/ Reel 2018</span></h1>
			
			<a id="thumbHomeReel" href="https://player.vimeo.com/video/256234400?color=000&title=0&byline=0&portrait=0&autoplay=1">
				<img src="https://gustavopera.com/wp-content/uploads/2018/03/thumbnail-reel-2018.jpg" width="100%" height="auto">
				<span class="transitions">ASSISTA AGORA</span>
			</a>
			
			<div id="videoHomeReel">
				<a id="closeHomeReel">fechar</a>
				<iframe src="" width="100%" height="auto" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
			<style type="text/css">
				.site-content {
					margin-right: 50px;
				}

				#thumbHomeReel {
					display: block;
					position: relative;
					overflow: hidden;
					cursor: pointer;
				}
				#thumbHomeReel img {}
				#thumbHomeReel span {
					background-color: #000;
					font-family: "Oswald", sans-serif;
					font-size: 18px;
					font-style: italic;
					color: #FFF;
					letter-spacing: 5px;
					padding: 4px 25px 5px 25px;
					border-radius: 100px;
					position: absolute;
					top: 80%;
					left: 50%;
					transform: translateX(-50%) translateY(-50%);
					box-shadow: 0 10px 20px rgba(0,0,0,0.5);
				}
				#thumbHomeReel:hover span {
					color: #FFCE00;
					top: 79%;
				}

				#titleShowReel {
				    margin-top: 45px;
					margin-bottom: 15px;
				}
				#videoHomeReel {
					background-color: #FFF;
					display: none;
					width: 100%;
					height: 100%;
					position: fixed;
					top: 50%;
					left: 50%;
					z-index: 9999;
					transform: translateX(-50%) translateY(-50%);
					/*box-shadow: 0 20px 40px rgba(0,0,0,0.23);*/
				}
				#videoHomeReel #closeHomeReel {
					background-color: #000;
					font-family: "Oswald", sans-serif;
					font-size: 15px;
					font-style: italic;
					color: #FFF;
					letter-spacing: 5px;
					padding: 3px 15px 4px 15px;
					position: absolute;
					border-radius: 100px;
					position: absolute;
					top: 40px;
					right: 40px;
					z-index: 2;
					box-shadow: 0 10px 20px rgba(0,0,0,0.5);
					cursor: pointer;
				}
				#videoHomeReel iframe {
					width: 65.5%;
    				height: 79.5%;
				    position: relative;
				    z-index: 1;
				    transform: translateY(-50%) translateX(-50%);
				    top: 50%;
				    left: 50%;
				    /*box-shadow: 0 15px 50px rgba(0,0,0,0.3);*/
				}
			</style>
			
			<div class="content-width">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-8" itemprop="mainContentOfPage"></div>
						<div class="col-md-4"></div>
					</div>
				</div>
			</div>
			
	  </main><!-- #main -->
	  
	</div><!-- #primary -->
</div><!-- .site-content -->

<?php get_footer(); ?>