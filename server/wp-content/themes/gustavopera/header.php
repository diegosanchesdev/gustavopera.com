<?php
/**
 * The header for our theme.
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @package Morphology Lite
 */

 $logo_upload = get_option( 'logo_upload' );
 
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<link href="https://fonts.googleapis.com/css?family=Oswald|Sanchez" rel="stylesheet">
<style type="text/css">
	html {
		margin-top: 0px !important;
	}
</style>
</head>

<body <?php body_class('loading'); ?>>
<div id="loading" class="transitions">
	<span>Carregando</span>
</div>
<style type="text/css">
	#loading {
		background-color: #FFF;
		display: none;
		width: 100%;
		height: 100%;
		position: fixed;
		top: 0;
		left: 0;
		z-index: 999;
		cursor: wait;
	}
	#loading span {
		font-size: 60px;
		color: #CCC;
	}
</style>
<div id="loading"></div>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'morphology-lite' ); ?></a>

	<div id="sidebar" class="sidebar" <?php if ( get_header_image() ) : ?>style="background-image: url('<?php header_image(); ?>') <?php endif; ?>">
	
	
	
	
		<header id="masthead" class="site-header" role="banner">
			<div id="site-branding" class="clearfix">
				<div id="site-branding-inner">
                               
              		<div class="site-logo" itemscope itemtype="http://schema.org/Organization">
                    	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" itemprop="url">
                        	<?php morphology_lite_custom_logo(); ?>
                        </a>    
                    </div>                
                        
            
                 <?php  if( esc_attr(get_theme_mod( 'show_site_title', 1 ) ) ) :  ?>
                    
                        <div class="site-title" itemprop="headline"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div>
                        
                 <?php endif; ?>
                    
                    <?php  if ( esc_attr(get_theme_mod( 'show_tagline', 1 ) ) ) : ?>
                        <?php  $description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) : ?>
							<div class="site-description" itemprop="description"><?php echo $description; ?></div>
							<style type="text/css">
								.site-description {
									display: none;
									margin: 10px 0 0 180px;
								}
							</style>
						<?php endif; ?>
               		<?php endif;  ?>            
            
				

				</div>
			</div><!-- .site-branding -->
			
			<nav id="site-navigation" class="main-navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
                <div class="toggle-container">
                        <button class="menu-toggle"><?php esc_html_e( 'Menu', 'morphology-lite' ); ?></button>
                </div>
                              
				<?php
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'menu_class'     => 'nav-menu',
						'fallback_cb'    => false,
					 ) );
				?>
				

				<div id="socialMediaHeader">
					<a href="https://www.facebook.com/gustavopera/" class="genericon genericon-facebook" target="_blank"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjI0cHgiIGhlaWdodD0iMjRweCIgdmlld0JveD0iMCAwIDk2LjEyNCA5Ni4xMjMiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDk2LjEyNCA5Ni4xMjM7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8cGF0aCBkPSJNNzIuMDg5LDAuMDJMNTkuNjI0LDBDNDUuNjIsMCwzNi41Nyw5LjI4NSwzNi41NywyMy42NTZ2MTAuOTA3SDI0LjAzN2MtMS4wODMsMC0xLjk2LDAuODc4LTEuOTYsMS45NjF2MTUuODAzICAgYzAsMS4wODMsMC44NzgsMS45NiwxLjk2LDEuOTZoMTIuNTMzdjM5Ljg3NmMwLDEuMDgzLDAuODc3LDEuOTYsMS45NiwxLjk2aDE2LjM1MmMxLjA4MywwLDEuOTYtMC44NzgsMS45Ni0xLjk2VjU0LjI4N2gxNC42NTQgICBjMS4wODMsMCwxLjk2LTAuODc3LDEuOTYtMS45NmwwLjAwNi0xNS44MDNjMC0wLjUyLTAuMjA3LTEuMDE4LTAuNTc0LTEuMzg2Yy0wLjM2Ny0wLjM2OC0wLjg2Ny0wLjU3NS0xLjM4Ny0wLjU3NUg1Ni44NDJ2LTkuMjQ2ICAgYzAtNC40NDQsMS4wNTktNi43LDYuODQ4LTYuN2w4LjM5Ny0wLjAwM2MxLjA4MiwwLDEuOTU5LTAuODc4LDEuOTU5LTEuOTZWMS45OEM3NC4wNDYsMC44OTksNzMuMTcsMC4wMjIsNzIuMDg5LDAuMDJ6IiBmaWxsPSIjMDAwMDAwIi8+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" /></a>

					<a href="https://www.instagram.com/gu_pera/" class="genericon genericon-instagram" target="_blank"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjI0cHgiIGhlaWdodD0iMjRweCI+CjxnPgoJPGc+CgkJPHBhdGggZD0iTTM3My42NTksMEgxMzguMzQxQzYyLjA2LDAsMCw2Mi4wNiwwLDEzOC4zNDF2MjM1LjMxOEMwLDQ0OS45NCw2Mi4wNiw1MTIsMTM4LjM0MSw1MTJoMjM1LjMxOCAgICBDNDQ5Ljk0LDUxMiw1MTIsNDQ5Ljk0LDUxMiwzNzMuNjU5VjEzOC4zNDFDNTEyLDYyLjA2LDQ0OS45NCwwLDM3My42NTksMHogTTQ3MC42MzYsMzczLjY1OSAgICBjMCw1My40NzMtNDMuNTAzLDk2Ljk3Ny05Ni45NzcsOTYuOTc3SDEzOC4zNDFjLTUzLjQ3MywwLTk2Ljk3Ny00My41MDMtOTYuOTc3LTk2Ljk3N1YxMzguMzQxICAgIGMwLTUzLjQ3Myw0My41MDMtOTYuOTc3LDk2Ljk3Ny05Ni45NzdoMjM1LjMxOGM1My40NzMsMCw5Ni45NzcsNDMuNTAzLDk2Ljk3Nyw5Ni45NzdWMzczLjY1OXoiIGZpbGw9IiMwMDAwMDAiLz4KCTwvZz4KPC9nPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0zNzAuNTg2LDIzOC4xNDFjLTMuNjQtMjQuNTQ3LTE0LjgzOS00Ni43OTUtMzIuMzg2LTY0LjM0MmMtMTcuNTQ3LTE3LjU0Ni0zOS43OTUtMjguNzQ2LTY0LjM0MS0zMi4zODUgICAgYy0xMS4xNzYtMS42NTctMjIuNTA3LTEuNjU3LTMzLjY4MiwwYy0zMC4zMzYsNC40OTktNTcuMTAzLDIwLjU0MS03NS4zNzIsNDUuMTcyYy0xOC4yNjksMjQuNjMxLTI1Ljg1NCw1NC45MDMtMjEuMzU1LDg1LjIzNyAgICBjNC40OTksMzAuMzM1LDIwLjU0MSw1Ny4xMDIsNDUuMTcyLDc1LjM3MmMxOS45OTYsMTQuODMxLDQzLjcwNiwyMi42MTksNjguMTUzLDIyLjYxOWM1LjY2NywwLDExLjM3NS0wLjQxOCwxNy4wODMtMS4yNjUgICAgYzMwLjMzNi00LjQ5OSw1Ny4xMDMtMjAuNTQxLDc1LjM3Mi00NS4xNzJDMzY3LjUsMjk4Ljc0NywzNzUuMDg1LDI2OC40NzYsMzcwLjU4NiwyMzguMTQxeiBNMjY3Ljc5MSwzMjcuNjMyICAgIGMtMTkuNDA1LDIuODgyLTM4Ljc3LTEuOTczLTU0LjUyNy0xMy42NmMtMTUuNzU3LTExLjY4Ny0yNi4wMTktMjguODExLTI4Ljg5Ni00OC4yMTZjLTIuODc4LTE5LjQwNSwxLjk3My0zOC43NywxMy42Ni01NC41MjcgICAgYzExLjY4OC0xNS43NTcsMjguODExLTI2LjAxOSw0OC4yMTctMjguODk3YzMuNTc0LTAuNTMsNy4xNzMtMC43OTUsMTAuNzcyLTAuNzk1czcuMTk5LDAuMjY1LDEwLjc3MywwLjc5NiAgICBjMzIuMjMxLDQuNzc5LDU3LjA5OCwyOS42NDUsNjEuODc4LDYxLjg3N0MzMzUuNjA4LDI4NC4yNjgsMzA3Ljg1MSwzMjEuNjkyLDI2Ny43OTEsMzI3LjYzMnoiIGZpbGw9IiMwMDAwMDAiLz4KCTwvZz4KPC9nPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik00MDAuMDQ5LDExMS45NTFjLTMuODUyLTMuODUxLTkuMTgzLTYuMDU4LTE0LjYyNS02LjA1OGMtNS40NDIsMC0xMC43NzMsMi4yMDYtMTQuNjI1LDYuMDU4ICAgIGMtMy44NTEsMy44NTItNi4wNTgsOS4xNzQtNi4wNTgsMTQuNjI1YzAsNS40NTEsMi4yMDcsMTAuNzczLDYuMDU4LDE0LjYyNWMzLjg1MiwzLjg1MSw5LjE4Myw2LjA1OCwxNC42MjUsNi4wNTggICAgYzUuNDQyLDAsMTAuNzczLTIuMjA2LDE0LjYyNS02LjA1OGMzLjg1MS0zLjg1Miw2LjA1OC05LjE4Myw2LjA1OC0xNC42MjVDNDA2LjEwNywxMjEuMTMzLDQwMy45LDExNS44MDIsNDAwLjA0OSwxMTEuOTUxeiIgZmlsbD0iIzAwMDAwMCIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" /></a>

					<a href="https://www.youtube.com/channel/UCVM0ROo31_entY28Z-j5oDA/" class="genericon genericon-youtube" target="_blank"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjI0cHgiIGhlaWdodD0iMjRweCIgdmlld0JveD0iMCAwIDkwIDkwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA5MCA5MDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8Zz4KCTxwYXRoIGlkPSJZb3VUdWJlIiBkPSJNNzAuOTM5LDY1LjgzMkg2NmwwLjAyMy0yLjg2OWMwLTEuMjc1LDEuMDQ3LTIuMzE4LDIuMzI2LTIuMzE4aDAuMzE1YzEuMjgyLDAsMi4zMzIsMS4wNDMsMi4zMzIsMi4zMTggICBMNzAuOTM5LDY1LjgzMnogTTUyLjQxMyw1OS42ODRjLTEuMjUzLDAtMi4yNzgsMC44NDItMi4yNzgsMS44NzNWNzUuNTFjMCwxLjAyOSwxLjAyNSwxLjg2OSwyLjI3OCwxLjg2OSAgIGMxLjI1OCwwLDIuMjg0LTAuODQsMi4yODQtMS44NjlWNjEuNTU3QzU0LjY5Nyw2MC41MjUsNTMuNjcxLDU5LjY4NCw1Mi40MTMsNTkuNjg0eiBNODIuNSw1MS44Nzl2MjYuNTQ0ICAgQzgyLjUsODQuNzksNzYuOTc5LDkwLDcwLjIzLDkwSDE5Ljc3MUMxMy4wMiw5MCw3LjUsODQuNzksNy41LDc4LjQyM1Y1MS44NzljMC02LjM2Nyw1LjUyLTExLjU3OCwxMi4yNzEtMTEuNTc4SDcwLjIzICAgQzc2Ljk3OSw0MC4zMDEsODIuNSw0NS41MTIsODIuNSw1MS44Nzl6IE0yMy4xMzcsODEuMzA1bC0wLjAwNC0yNy45NjFsNi4yNTUsMC4wMDJ2LTQuMTQzbC0xNi42NzQtMC4wMjV2NC4wNzNsNS4yMDUsMC4wMTV2MjguMDM5ICAgSDIzLjEzN3ogTTQxLjg4Nyw1Ny41MDloLTUuMjE1djE0LjkzMWMwLDIuMTYsMC4xMzEsMy4yNC0wLjAwOCwzLjYyMWMtMC40MjQsMS4xNTgtMi4zMywyLjM4OC0zLjA3MywwLjEyNSAgIGMtMC4xMjYtMC4zOTYtMC4wMTUtMS41OTEtMC4wMTctMy42NDNsLTAuMDIxLTE1LjAzNGgtNS4xODZsMC4wMTYsMTQuNzk4YzAuMDA0LDIuMjY4LTAuMDUxLDMuOTU5LDAuMDE4LDQuNzI5ICAgYzAuMTI3LDEuMzU3LDAuMDgyLDIuOTM5LDEuMzQxLDMuODQzYzIuMzQ2LDEuNjksNi44NDMtMC4yNTIsNy45NjgtMi42NjhsLTAuMDEsMy4wODNsNC4xODgsMC4wMDVMNDEuODg3LDU3LjUwOUw0MS44ODcsNTcuNTA5eiAgICBNNTguNTcsNzQuNjA3TDU4LjU1OSw2Mi4xOGMtMC4wMDQtNC43MzYtMy41NDctNy41NzItOC4zNTYtMy43NGwwLjAyMS05LjIzOWwtNS4yMDksMC4wMDhsLTAuMDI1LDMxLjg5bDQuMjg0LTAuMDYybDAuMzktMS45ODYgICBDNTUuMTM3LDg0LjA3Miw1OC41NzgsODAuNjMxLDU4LjU3LDc0LjYwN3ogTTc0Ljg5MSw3Mi45NmwtMy45MSwwLjAyMWMtMC4wMDIsMC4xNTUtMC4wMDgsMC4zMzQtMC4wMSwwLjUyOXYyLjE4MiAgIGMwLDEuMTY4LTAuOTY1LDIuMTE5LTIuMTM3LDIuMTE5aC0wLjc2NmMtMS4xNzQsMC0yLjEzOS0wLjk1MS0yLjEzOS0yLjExOVY3NS40NXYtMi40di0zLjA5N2g4Ljk1NHYtMy4zNyAgIGMwLTIuNDYzLTAuMDYzLTQuOTI1LTAuMjY3LTYuMzMzYy0wLjY0MS00LjQ1NC02Ljg5My01LjE2MS0xMC4wNTEtMi44ODFjLTAuOTkxLDAuNzEyLTEuNzQ4LDEuNjY1LTIuMTg4LDIuOTQ1ICAgYy0wLjQ0NCwxLjI4MS0wLjY2NSwzLjAzMS0wLjY2NSw1LjI1NHY3LjQxQzYxLjcxNCw4NS4yOTYsNzYuNjc2LDgzLjU1NSw3NC44OTEsNzIuOTZ6IE01NC44MzMsMzIuNzMyICAgYzAuMjY5LDAuNjU0LDAuNjg3LDEuMTg0LDEuMjU0LDEuNTg0YzAuNTYsMC4zOTQsMS4yNzYsMC41OTIsMi4xMzQsMC41OTJjMC43NTIsMCwxLjQxOC0wLjIwMywxLjk5OC0wLjYyMiAgIGMwLjU3OC0wLjQxNywxLjA2NS0xLjA0LDEuNDYzLTEuODcxbC0wLjA5OSwyLjA0Nmg1LjgxM1Y5Ljc0SDYyLjgydjE5LjI0YzAsMS4wNDItMC44NTgsMS44OTUtMS45MDcsMS44OTUgICBjLTEuMDQzLDAtMS45MDQtMC44NTMtMS45MDQtMS44OTVWOS43NGgtNC43NzZ2MTYuNjc0YzAsMi4xMjQsMC4wMzksMy41NCwwLjEwMiw0LjI1OEM1NC40LDMxLjM4NSw1NC41NjQsMzIuMDY5LDU0LjgzMywzMi43MzJ6ICAgIE0zNy4yMTcsMTguNzdjMC0yLjM3MywwLjE5OC00LjIyNiwwLjU5MS01LjU2MmMwLjM5Ni0xLjMzMSwxLjEwNy0yLjQwMSwyLjEzNy0zLjIwOGMxLjAyNy0wLjgxMSwyLjM0Mi0xLjIxNywzLjk0MS0xLjIxNyAgIGMxLjM0NSwwLDIuNDk3LDAuMjY0LDMuNDU5LDAuNzgxYzAuOTY3LDAuNTIsMS43MTMsMS4xOTUsMi4yMywyLjAyOGMwLjUyNywwLjgzNiwwLjg4NSwxLjY5NSwxLjA3NiwyLjU3NCAgIGMwLjE5NSwwLjg5MSwwLjI5MSwyLjIzNSwwLjI5MSw0LjA0OHY2LjI1MmMwLDIuMjkzLTAuMDkyLDMuOTgtMC4yNzEsNS4wNTFjLTAuMTc3LDEuMDc0LTAuNTU3LDIuMDctMS4xNDYsMy4wMDQgICBjLTAuNTgsMC45MjQtMS4zMjksMS42MTUtMi4yMzcsMi4wNTZjLTAuOTE4LDAuNDQ1LTEuOTY4LDAuNjYzLTMuMTU0LDAuNjYzYy0xLjMyNSwwLTIuNDQxLTAuMTgzLTMuMzYxLTAuNTY1ICAgYy0wLjkyMy0wLjM4LTEuNjM2LTAuOTUzLTIuMTQ0LTEuNzE0Yy0wLjUxMy0wLjc2Mi0wLjg3NC0xLjY5LTEuMDkyLTIuNzcyYy0wLjIxOS0xLjA4MS0wLjMyMy0yLjcwNy0wLjMyMy00Ljg3NEwzNy4yMTcsMTguNzcgICBMMzcuMjE3LDE4Ljc3eiBNNDEuNzcsMjguNTljMCwxLjQsMS4wNDIsMi41NDMsMi4zMTEsMi41NDNjMS4yNywwLDIuMzA4LTEuMTQzLDIuMzA4LTIuNTQzVjE1LjQzYzAtMS4zOTgtMS4wMzgtMi41NDEtMi4zMDgtMi41NDEgICBjLTEuMjY5LDAtMi4zMTEsMS4xNDMtMi4zMTEsMi41NDFWMjguNTl6IE0yNS42ODIsMzUuMjM1aDUuNDg0bDAuMDA2LTE4Ljk2bDYuNDgtMTYuMjQyaC01Ljk5OGwtMy40NDUsMTIuMDY0TDI0LjcxNSwwaC01LjkzNiAgIGw2Ljg5NCwxNi4yODRMMjUuNjgyLDM1LjIzNXoiIGZpbGw9IiMwMDAwMDAiLz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" /></a>

					<a href="https://vimeo.com/gustavopera" class="genericon genericon-vimeo" target="_blank"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjI0cHgiIGhlaWdodD0iMjRweCIgdmlld0JveD0iMCAwIDQzMC4xMTggNDMwLjExOCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDMwLjExOCA0MzAuMTE4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPHBhdGggaWQ9IlZpbWVvIiBkPSJNMzY3LjI0MywyOC43NTRjLTU5Ljc5NS0xLjk1MS0xMDAuMjU5LDMxLjU5MS0xMjEuNDQ3LDEwMC42NjRjMTAuOTEyLTQuNDk0LDIxLjUxNi02Ljc2MiwzMS44NTgtNi43NjIgICBjMjEuODA0LDAsMzEuNDU1LDEyLjIzNywyOC44NzksMzYuNzc2Yy0xLjI3OCwxNC44Ni0xMC45MTEsMzYuNDgyLTI4Ljg3OSw2NC44NThjLTE4LjAzOSwyOC40MjMtMzEuNTEzLDQyLjYxLTQwLjQ2NCw0Mi42MSAgIGMtMTEuNjIxLDAtMjIuMTk5LTIxLjk1OC0zMS44NTctNjUuODJjLTMuMjM5LTEyLjkxOC05LjAzMS00NS44MTItMTcuMzI0LTk4Ljc2NWMtNy43NzUtNDkuMDQ2LTI4LjMyLTcxLjk2Mi02MS43MjctNjguNzQxICAgQzExMi4xNSwzNC44NzMsOTAuOTgsNDcuODE1LDYyLjcyNiw3Mi4zMDhDNDIuMTEzLDkxLjAzMiwyMS4yMjgsMTA5Ljc2MSwwLDEyOC40NzFsMjAuMjI1LDI2LjExMiAgIGMxOS4zMDMtMTMuNTYyLDMwLjU5NS0yMC4zMTEsMzMuNzMxLTIwLjMxMWMxNC44MDIsMCwyOC42MjUsMjMuMjE5LDQxLjQ4OCw2OS42NTFjMTEuNTMsNDIuNjQ0LDIzLjE1OCw4NS4yMywzNC43NDQsMTI3LjgxMiAgIGMxNy4yNTYsNDYuNDY2LDM4LjUyOSw2OS43MDgsNjMuNTUyLDY5LjcwOGM0MC40NzMsMCw5MC4wMjgtMzguMDY1LDE0OC40NjktMTE0LjIyM2M1Ni41MzctNzIuOTA5LDg1LjcyNS0xMzAuMzUyLDg3LjY5NC0xNzIuMzQxICAgQzQzMi40OTgsNTguNzY0LDQxMS42MTMsMzAuMDI4LDM2Ny4yNDMsMjguNzU0eiIgZmlsbD0iIzAwMDAwMCIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" /></a>
				</div>
				<style type="text/css">
					#socialMediaHeader {
						margin-top: 20%;
					}
					#socialMediaHeader a {
						text-align: center;
						display: block;
						width: 40px;
						height: 40px;
						line-height: 50px;
						padding: 0 !important;
						margin-left: auto;
						margin-right: 20px;
					}
					#socialMediaHeader a:hover {
						background-color: #FFF !important;
						box-shadow: none !important;
					}
					#socialMediaHeader a img {
						display: inline-block;
						width: 20px;
						height: 20px;
					}
					@media screen and (max-width: 768px) {
						#socialMediaHeader {
							text-align: center;
							margin-top: 5%;
							margin-bottom: 5%;
						}
						#socialMediaHeader a {
							display: inline-block;
							margin-left: 5px;
							margin-right: 5px;
						}
					}
				</style>



            </nav><!-- #site-navigation -->
            <style type="text/css">
            	.main-navigation {
            		padding: 4% 6%;
            	}
            </style>
			
		</header><!-- .site-header -->	
	
	<?php  if ( esc_attr(get_theme_mod( 'show_columnfooter', 1 ) ) ) : ?>
		<footer class="column-footer hidden-lg-down">  
			<?php if ( has_nav_menu( 'social' ) ) :
				echo '<nav class="social-menu">';
				wp_nav_menu( array(
				'theme_location' => 'social', 
				'depth' => 1, 
				'container' => false, 
				'menu_class' => 'social-icons', 
				'link_before' => '<span class="screen-reader-text">', 
				'link_after' => '</span>',
				) );
			echo '</nav>';
			endif; ?>

			<div class="site-info">
			<?php esc_html_e('Copyright &copy;', 'morphology-lite'); ?> 
			<?php echo date('Y'); ?> <?php echo esc_html(get_theme_mod( 'copyright', 'Your Name' )); ?>.<br><?php esc_html_e('All rights reserved.', 'morphology-lite'); ?>
			</div> 
			
		</footer><!-- .site-footer -->
	<?php endif; ?> 

</div><!-- .sidebar -->
 